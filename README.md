TWBS
====

Drupal Bootstrap Remix (a.k.a. TWBS) is an abstraction layer for Drupal
core, which target to replace ENTIRE core themeable layer into
mobile-first design and [Bootstrap](http://getbootstrap.com) friendly.

### Related modules

-   [TWBS Bootstrap](https://drupal.org/project/twbs_bootstrap)
-   [TWBS Contextual](https://drupal.org/project/twbs_contextual)
-   [TWBS Font Awesome](https://drupal.org/project/twbs_fontawesome)
-   [TWBS Holder](https://drupal.org/project/twbs_holder)
-   [TWBS jQuery](https://drupal.org/project/twbs_jquery)
-   [TWBS LESS](https://drupal.org/project/twbs_less)
-   [TWBS Navbar](https://drupal.org/project/twbs_navbar)
-   [TWBS Seven](https://drupal.org/project/twbs_seven)
-   [TWBS Stark](https://drupal.org/project/twbs_stark)

### Author

-   Developed by [Edison Wong](http://drupal.org/user/33940).
-   Sponsored by [PantaRei Design](http://drupal.org/node/1741828).
