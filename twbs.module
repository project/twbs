<?php

/**
 * @file
 * Drupal Bootstrap Remix.
 */

/**
 * Implements hook_help().
 */
function twbs_help($path, $arg) {
  $output = '';

  switch ($path) {
  case 'admin/help#twbs':
    return '<p>' . t('Drupal Bootstrap Remix.') . '</p>';
  }

  return $output;
}

/**
 * Implements hook_css_alter().
 */
function twbs_css_alter(&$css) {
  $path = drupal_get_path('module', 'twbs');

  // Automatically replace CSS that conflict with TWBS.
  $files = file_scan_directory($path . '/css', '/\.css$/');
  foreach ($files as $file) {
    $hook = preg_replace(
      '/^.*\/css\/(modules|themes)*\/*/',
      '',
      $file->uri
    );
    $pattern = '/' . preg_quote($hook, '/') . '$/';

    $matches = preg_grep($pattern, array_keys($css));
    if ($key = reset($matches)) {
      // Only restore settings with replacement if placeholder is not empty.
      if (filesize($file->uri) > 0) {
        $css[$file->uri] = $css[$key];
        $css[$file->uri]['data'] = $file->uri;
      }

      // Exclude original CSS.
      unset($css[$key]);
    }
  }

  // Automatically replace LESS that conflict with TWBS.
  $files = file_scan_directory($path . '/less', '/\.less$/');
  foreach ($files as $file) {
    $hook = preg_replace(
      array('/^.*\/less\/(modules|themes)*\/*/', '/\.less$/'),
      array('', '.css'),
      $file->uri
    );
    $pattern = '/' . preg_quote($hook, '/') . '$/';

    $matches = preg_grep($pattern, array_keys($css));
    if ($key = reset($matches)) {
      // Only restore settings with replacement if placeholder is not empty.
      if (filesize($file->uri) > 0) {
        $css[$file->uri] = $css[$key];
        $css[$file->uri]['data'] = $file->uri;
      }

      // Exclude original CSS.
      unset($css[$key]);
    }
  }
}

/**
 * Implements hook_js_alter().
 */
function twbs_js_alter(&$javascript) {
  $path = drupal_get_path('module', 'twbs');

  // Automatically replace JS that conflict with TWBS.
  $files = file_scan_directory($path . '/js', '/\.js$/');
  foreach ($files as $file) {
    $hook = preg_replace(
      '/^.*\/js\/(modules|themes)*\/*/',
      '',
      $file->uri
    );
    $pattern = '/' . preg_quote($hook, '/') . '$/';

    $matches = preg_grep($pattern, array_keys($javascript));
    if ($key = reset($matches)) {
      // Only restore setting with replacement if placeholder is not empty.
      if (filesize($file->uri) > 0) {
        $javascript[$file->uri] = $javascript[$key];
        $javascript[$file->uri]['data'] = $file->uri;
      }

      // Exclude original JS.
      unset($javascript[$key]);
    }
  }
}

/**
 * Implements hook_theme_registry_alter().
 */
function twbs_theme_registry_alter(&$theme_registry) {
  $path = drupal_get_path('module', 'twbs');

  // Scan and prepare all theme_*() replacement.
  $functions = array();
  // @see http://stackoverflow.com/a/406408
  $files = file_scan_directory($path . '/theme', '/\.inc$/');
  foreach ($files as $file) {
    $hook = preg_replace('/^twbs_/', '', $file->name);
    $functions[$hook] = $file->uri;
  }

  // Scan and prepare all template replacement.
  $templates = array();
  $files = file_scan_directory($path . '/template', '/\.tpl\.php$/');
  foreach ($files as $file) {
    $hook = preg_replace(
      array('/-/', '/\.tpl$/'),
      array('_', ''),
      $file->name
    );
    $templates[$hook] = preg_replace('/\.tpl\.php$/', '', $file->uri);
  }

  // Scan and prepare all template_preprocess_*() replacement.
  $preprocesses = array();
  $files = file_scan_directory($path . '/preprocess', '/\.inc$/');
  foreach ($files as $file) {
    $hook = preg_replace('/^twbs_preprocess_/', '', $file->name);
    $preprocesses[$hook] = $file->uri;
  }

  // Scan and prepare all template_process_*() replacement.
  $processes = array();
  $files = file_scan_directory($path . '/process', '/\.inc$/');
  foreach ($files as $file) {
    $hook = preg_replace('/^twbs_process_/', '', $file->name);
    $processes[$hook] = $file->uri;
  }

  // Preform all replacement to theme registry.
  foreach ($theme_registry as $hook => &$value) {
    if (isset($functions[$hook]) && $value['type'] == 'module') {
      $value['function'] = preg_replace(
        '/^theme_/',
        'twbs_',
        $value['function']
      );
      $value['includes'][] = $functions[$hook];
    }

    if (isset($templates[$hook]) && $value['type'] == 'module') {
      $value['template'] = $templates[$hook];
      // Template already coming with full path, so unset path.
      unset($value['path']);
    }

    if (isset($preprocesses[$hook])) {
      $value['preprocess functions'] = preg_replace(
        '/^template_preprocess_/',
        'twbs_preprocess_',
        $value['preprocess functions']
      );
      $value['includes'][] = $preprocesses[$hook];
    }

    if (isset($processes[$hook])) {
      $value['process functions'] = preg_replace(
        '/^template_process_/',
        'twbs_process_',
        $value['process functions']
      );
      $value['includes'][] = $processes[$hook];
    }
  }
}

/**
 * Implements hook_menu_alter().
 */
function twbs_menu_alter(&$items) {
  $path = drupal_get_path('module', 'twbs');

  // Scan and prepare all page callback replacement.
  $callbacks = array();
  $files = file_scan_directory($path . '/menu', '/\.inc$/');
  foreach ($files as $file) {
    $hook = preg_replace('/^twbs_menu_(.*)_alter$/', '$1', $file->name);
    $callbacks[$hook] = $file;
  }

  // Preform all replacement to menu.
  foreach ($items as $url => &$item) {
    if (isset($item['page callback'])) {
      $hook = $item['page callback'];
      if (isset($callbacks[$hook])) {
        $file = $callbacks[$hook];
        $item['page callback'] = 'twbs_menu_' . $hook . '_alter';
        $item['file'] = $file->filename;
        $item['file path'] = str_replace('/' . $file->filename, '', $file->uri);
      }
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function twbs_form_alter(&$form, &$form_state, $form_id) {
  $path = drupal_get_path('module', 'twbs');

  // Manual scan and trigger form alter.
  $files = file_scan_directory(
    $path . '/form',
    '/twbs_form_' . $form_id . '_alter\.inc$/'
  );
  foreach ($files as $file) {
    require_once $file->uri;
    $function = $file->name;
    $function($form, $form_state, $form_id);
  }
}
